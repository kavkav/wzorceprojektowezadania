package savefile;

import java.io.File;
import java.io.IOException;

public class SaveFile {
    private static SaveFile saveFile;

    private SaveFile() {
    }

    public static SaveFile getInstance(){
        if (saveFile == null) {
            synchronized (SaveFile.class) {
                if (saveFile == null) {
                    saveFile = new SaveFile();
                }
            }
        }
        return saveFile;
    }

    public void save(String path) {
        File file = new File(path);
        try {
            file.createNewFile();
        } catch (IOException e) {
        }

    }
}
