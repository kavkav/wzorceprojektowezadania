package carfactory;

public class WolksvagenFactory  extends CarFactory{
    public WolksvagenFactory(String address) {
        super(address, "wolksvagen");
    }

    @Override
    Car constructCar(String model) throws IllegalArgumentException {
        if("golf".equals(model)){
            return new Golf();
        }else if("polo".equals(model)){
            return new Polo();
        }else{
            throw new IllegalArgumentException();
        }
    }
}
