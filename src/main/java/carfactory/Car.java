package carfactory;

import java.time.LocalDateTime;

abstract class Car {
    private String productionDate;
    private double price;
    private boolean working;
    public void testCar(){
        working = true;
    }

    public Car(double price) {
        this.productionDate = LocalDateTime.now().toString();
        this.price = price;
    }

    public String getProductionDate() {
        return productionDate;
    }

    public double getPrice() {
        return price;
    }

    public boolean isWorking() {
        return working;
    }
}
