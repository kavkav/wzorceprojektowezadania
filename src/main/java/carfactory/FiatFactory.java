package carfactory;

public class FiatFactory extends CarFactory{
    public FiatFactory(String address) {
        super(address, "fiat");
    }

    @Override
    Car constructCar(String model) throws IllegalArgumentException {
        if("punto".equals(model)){
            return new Punto();
        }else if("maluch".equals(model) || "126p".equals(model)){
            return new Maluch();
        }else{
            throw new IllegalArgumentException();
        }
    }
}
