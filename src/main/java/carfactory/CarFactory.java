package carfactory;

import java.util.ArrayList;
import java.util.List;

abstract class CarFactory {
    private String address;
    private String brand;

    CarFactory(String address, String brand) {
            this.address = address;
            this.brand = brand;
        }

        public double placeOrder(String model, int count) throws IllegalArgumentException{
            List<Car> order = new ArrayList<>();
            Car car = null;
            for (int i = 0; i < count; i++) {
                car = constructCar(model);
                car.testCar();
                if(car.isWorking()) {
                    order.add(car);
                }
            }
            int sum = 0;
            for(Car readyCar : order){
                sum += readyCar.getPrice();
            }
            return sum;
        }

        abstract Car constructCar(String model);

        public String getAddress() {
            return address;
        }

        public String getBrand() {
            return brand;
        }
    }
