import carfactory.FiatFactory;
import carfactory.WolksvagenFactory;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestCarFactory {
    @Test
    public void CarFactoryOrderingTest(){
        FiatFactory fiatFactory1 = new FiatFactory("Katowice, ulica");
        assertEquals(4000, fiatFactory1.placeOrder("maluch", 2), 0);

        WolksvagenFactory wolksvagenFactory1 = new WolksvagenFactory("Wrocław, ulica");
        //wrong car model
        try {
            wolksvagenFactory1.placeOrder("punto", 1);
            fail();
        }catch (IllegalArgumentException e){ }
        //wrong car count
        assertEquals(0, fiatFactory1.placeOrder("punto", -1), 0);
    }
}
