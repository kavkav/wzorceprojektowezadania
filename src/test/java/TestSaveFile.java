import org.junit.Test;
import savefile.SaveFile;

import java.io.File;

import static junit.framework.TestCase.assertTrue;

public class TestSaveFile {
    @Test
    public void getInstanceTest(){
        SaveFile saveFile = SaveFile.getInstance();
        String stringPath = "totalniePlik.log";
        saveFile.save(stringPath);
        File file = new File(stringPath);
        assertTrue(file.exists());
        file.delete();
    }
}
